#!/bin/sh -e
test -n "$1" && 
if [[ "$1" == "-f" ]]; then
  rm -f public/.files
  echo "Generating all the files again"
fi
ssg6.sh src public "Blog of Hannupekka Sormunen" "https://sorhanp.gitlab.io"